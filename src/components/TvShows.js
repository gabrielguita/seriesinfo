import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchTvShows, fetchEpisodes } from '../actions/postAction';
import EpisodeDetails from './EpisodeDetails';
import EpisodeList from './EpisodeList';

class TvShows extends Component {
    UNSAFE_componentWillMount() {
        this.props.fetchTvShows();
        this.props.fetchEpisodes();
    }

    render() {
        const { tvShows, episodes } = this.props;

        return (
            <React.Fragment>
                {tvShows && <EpisodeDetails data={tvShows} />}
                {episodes && <EpisodeList data={episodes} />}
            </React.Fragment>
        )
    }
}

TvShows.propTypes = {
    fetchTvShows: PropTypes.func.isRequired,
    fetchEpisodes: PropTypes.func.isRequired,
    episodes: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
    tvShows: state.tvShows.items,
    episodes: state.episodes.items
});

export default connect(mapStateToProps, { fetchTvShows, fetchEpisodes })(TvShows);