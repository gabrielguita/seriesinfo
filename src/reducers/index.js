import { combineReducers } from 'redux';
import { postReducer, episodesReducer } from './postReducer';

export default combineReducers({
    tvShows: postReducer,
    episodes: episodesReducer
}); 
