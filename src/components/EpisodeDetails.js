import React from 'react';
import './../App.css';
import styled from 'styled-components'
import media from "styled-media-query";

export default function EpisodeDetails(data) {
    const episodeData = data.data;
    if (!episodeData) return null
    return (
        <EpisodeDetailsContainer>
            <h1>{episodeData.name}</h1>
            <ContentBox>
                <ContentBoxImg
                    src={episodeData.image && episodeData.image.medium}
                    alt=''>
                </ContentBoxImg>
                <Description dangerouslySetInnerHTML={{ __html: episodeData.summary }}>
                </Description>
            </ContentBox>
        </EpisodeDetailsContainer>
    )
}

const EpisodeDetailsContainer = styled.div` 
    ${media.lessThan("375px")`
        margin: 15px 0 0 0;
    `}
`
const Description = styled.div` 
    width: 100%;
`;

const ContentBox = styled.div` 
    width: 100%;
`;

const ContentBoxImg = styled.img` 
    float: left;
    margin-right: 20px;
    max-width: 180px;
    ${media.lessThan("375px")`
        max-width: 100%;
        width: 100%;
        float: none;
        margin: 0 auto;
    `}
`;