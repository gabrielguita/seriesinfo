import React, { Component } from 'react'
import EpisodeDetails from '../EpisodeDetails'
import { Link } from 'react-router-dom';
import './../../App.css';

class Episode extends Component {
    render() {
        const { data } = this.props && this.props.location
        if (!data) return null
        return (
            <React.Fragment>
                <div className="btnBack">
                    <Link to="/" >Back to The Powerpuff Girls</Link>
                </div>
                <EpisodeDetails data={data} />
            </React.Fragment>
        )
    }
}

export default Episode;