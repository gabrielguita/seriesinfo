import { FETCH_TV_SHOWS, FETCH_EPISODES } from './types';
const id = '6771';

export const fetchTvShows = () => dispatch => {
    fetch(`https://api.tvmaze.com/shows/${id}`)
    .then(res => res.json())
    .then(shows => 
        dispatch({
            type: FETCH_TV_SHOWS,
            payload: shows
        })
    )   
}

export const fetchEpisodes = () => dispatch => {
    fetch(`https://api.tvmaze.com/shows/${id}/episodes`)
        .then(res => res.json())
        .then(episodes => dispatch({
            type: FETCH_EPISODES,
            payload: episodes
        }));
}