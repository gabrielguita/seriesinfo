import { FETCH_TV_SHOWS, FETCH_EPISODES } from '../actions/types';

export const initialState = {
    items: [],
    item: {}
}

export const postReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TV_SHOWS:
            return {
                ...state,
                items: action.payload
            }

        default:
            return state;
    }
}

export const episodesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_EPISODES:
            return {
                ...state,
                items: action.payload
            }

        default:
            return state;
    }
}
