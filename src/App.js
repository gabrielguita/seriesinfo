import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Header from './components/layout/Header';
import './App.css';
import TvShows from './components/TvShows';
import Episode from './components/pages/Episode';
import { Provider } from 'react-redux';
import store from './store';

function App() {
  return (
    <Router>
      <Provider store={store}>
        <div className="App">
          <Header />
          <Route exact path="/" render={props => (
              <TvShows />
          )} />

          <Route path="/episode" component={Episode} />
        </div>
      </Provider>
    </Router>
  );
}

export default App;